package com.spotify.automation;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.sikuli.script.FindFailed;

import com.spotify.utils.DataProperties;

public class SearchTest {

	@Test
	public void searchSongsInLowerCaseTest() throws InterruptedException, FindFailed  {
		LoginScreen login = new LoginScreen();
		MainScreen mainWindow = login
				.enterLoginData(
						DataProperties.get("valid.login"), 
						DataProperties.get("valid.password"))
				.clickLogInSuccess();
		mainWindow
			.lookUpForSong(DataProperties.get("search.requestWithAllLowerCase"));
		//Verify if the main track is found
		assertTrue(mainWindow.isSearchResultFound(DataProperties.get("images.mainRecord")));
	}	
	
	@Test
	public void searchArtistTest() throws InterruptedException, FindFailed  {
		LoginScreen login = new LoginScreen();
		MainScreen mainWindow = login
				.enterLoginData(
						DataProperties.get("valid.login"), 
						DataProperties.get("valid.password"))
				.clickLogInSuccess();
		mainWindow
			.lookUpForSong(DataProperties.get("search.requestForArtist"));
		//Verify if the searched artist is found
		assertTrue(mainWindow.isSearchResultFound(DataProperties.get("images.artist")));
	}			
	
	@Test
	public void searchTrackTest() throws InterruptedException, FindFailed  {
		LoginScreen login = new LoginScreen();
		MainScreen mainWindow = login
				.enterLoginData(
						DataProperties.get("valid.login"), 
						DataProperties.get("valid.password"))
				.clickLogInSuccess();
		mainWindow
			.lookUpForSong(DataProperties.get("search.requestForTrack"));
		//Verify if the searched track is found
		assertTrue(mainWindow.isSearchResultFound(DataProperties.get("images.track")));
	}	
	
	@Test
	public void searchAlbumTest() throws InterruptedException, FindFailed  {
		LoginScreen login = new LoginScreen();
		MainScreen mainWindow = login
				.enterLoginData(
						DataProperties.get("valid.login"), 
						DataProperties.get("valid.password"))
				.clickLogInSuccess();
		mainWindow
			.lookUpForSong(DataProperties.get("search.requestForAlbum"));
		//Verify if the main track is found
		assertTrue(mainWindow.isSearchResultFound(DataProperties.get("images.album")));
	}		
	
	
}
