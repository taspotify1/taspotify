package com.spotify.automation;

import static org.testng.Assert.fail;

import java.io.IOException;

import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Sikulix;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.spotify.utils.DataProperties;

public class BaseTestCase {
	
	private Process app;
	
	@BeforeMethod
	public void setUp() throws InterruptedException, FindFailed {
//		openSpotify();
		System.out.println("inside setUp");
		app = run();
	}
	
	@AfterMethod
	public void tearDown() {
//		closeSpotify();
		stop();
	}

	private Process run() {
		try {
			System.out.println("inside processRun");
			System.out.println(Runtime.getRuntime().exec(DataProperties.get("spotify.path")));

			return Runtime.getRuntime().exec(DataProperties.get("spotify.path"));
			

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	private void stop() {
		app.destroy();
	}	
	
	/*private void openSpotify() throws InterruptedException, FindFailed, AWTException {
		App.open(DataProperties.get("spotify.path"));
	}
	
	private void closeSpotify() {
		//it should work like this but Sikuli crashed. don't 
		App.close("Spotify"); 
	}	*/
}
