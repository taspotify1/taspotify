package com.spotify.automation;


import java.io.File;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Match;
import org.sikuli.script.Mouse;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import com.spotify.utils.DataProperties;
import com.spotify.utils.Driver;

public class LoginScreen extends AbstractScreen {
	private Pattern username;
	private Pattern password;
	private Pattern loginButton;
	private Pattern errorMessage;
	private Pattern loginWindow;
	private Screen window = new Screen(); 
	
	public LoginScreen() throws FindFailed {
		username = new Pattern(DataProperties.get("loginUserName"));
		password = new Pattern(DataProperties.get("loginPassword"));
		loginButton = new Pattern(DataProperties.get("loginSubmitButton"));
		loginWindow = new Pattern(DataProperties.get("loginWindow"));
		errorMessage = new Pattern(DataProperties.get("loginFailedLoginMessage"));
		
		//window = getDriver();
	}


	public LoginScreen enterLoginData(String user, String pass) throws FindFailed {
		System.out.println("Inside enterLoginData");
		
		window.type(username, user);
		window.type(password, pass);
		System.out.println("Exit enterLoginData");
		return this;
	}
	
	public LoginScreen clickLogIn() throws FindFailed {
		window.click(loginButton);
		return this;
	}
	
	public MainScreen clickLogIn(String vanish) throws FindFailed {
		window.click(loginButton);
		getDriver().waitVanish(loginWindow);
		return new MainScreen();
	}	
	
	public boolean isLoginWindowExist() {
		try {
			getDriver().find(loginWindow);
			return true;
		} catch (FindFailed e) {
			return false;
		}
	}
	
	public MainScreen clickLogInSuccess() throws FindFailed {
		Match loginButton_sr = window.find(loginButton);
		window.click();
		Pattern mainWindow = new Pattern(DataProperties.path("loginWindow.png"));
		window.wait(mainWindow, 5000);
		return new MainScreen();
	}	
	public boolean isErrorExist() {
		try {
			window.find(errorMessage);
			return true;
		} catch (FindFailed e) {
			return false;
		}
	}
	
}

