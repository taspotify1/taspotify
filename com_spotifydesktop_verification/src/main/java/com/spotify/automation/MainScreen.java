package com.spotify.automation;

import java.io.File;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import com.spotify.utils.DataProperties;
import com.spotify.utils.Driver;

public class MainScreen extends AbstractScreen {
	private Pattern search;
	private Pattern showAllResults;
	private Pattern songButtons;
	private Pattern firstSong;
	private Pattern mainWindow;
	
	private Region window;
	
	public MainScreen() throws FindFailed {
		search = new Pattern(DataProperties.get("mainSearch"));
		showAllResults = new Pattern(DataProperties.get("mainShowAllResults"));
		songButtons = new Pattern(DataProperties.get("mainSongButtons"));
		mainWindow = new Pattern(DataProperties.get("mainWindow"));
		firstSong = new Pattern(DataProperties.get("mainFoundRecord"));
		
		window = getDriver().wait(mainWindow.similar((float)0.30));		
	}
	
	public MainScreen lookUpForSong(String text) throws FindFailed {
		window.type(search, text);
		waitAndClick(showAllResults);
		return this;
	}
	
	public MainScreen clickFirstFoundSong() throws FindFailed {
		//here can be more complex logic how to identify first records
		//however, i will do just click for for simplification
		waitAndDoubleClick(firstSong);
		return this;
	}

	public boolean isSongPlaying(){
		try {
			window.find(songButtons.similar((float) 1.00));
			return true;
		} catch (FindFailed e) {
			return false;
		}	
	}

	private void waitAndClick(Pattern pattern) throws FindFailed {
		window.wait(pattern);
		window.click(pattern);
	}	
	
	private void waitAndDoubleClick(Pattern pattern) throws FindFailed {
		window.wait(pattern);
		window.doubleClick(pattern);
	}	
	
	public boolean isSearchResultFound(String searchFile) throws FindFailed {
		Pattern target = new Pattern(DataProperties.path(searchFile));
		Match sr = window.wait(target, 3000);
		if (sr != null) {
			
			return true;
		}
		return false;
	}
}
